package fracip.matdig.allinclusive.Clases;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Horario {


    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "horario_id")
    private long id;

    @ColumnInfo(name = "horario_idLocal")
    private long idLocal;

    @ColumnInfo(name = "horario_lunesAM")
    private String lunesAM;
    @ColumnInfo(name = "horario_lunesCM")
    private String lunesCM;
    @ColumnInfo(name = "horario_lunesAT")
    private String lunesAT;
    @ColumnInfo(name = "horario_lunesCT")
    private String lunesCT;

    @ColumnInfo(name = "horario_martesAM")
    private String martesAM;
    @ColumnInfo(name = "horario_martesCM")
    private String martesCM;
    @ColumnInfo(name = "horario_martesAT")
    private String martesAT;
    @ColumnInfo(name = "horario_martesCT")
    private String martesCT;

    @ColumnInfo(name = "horario_miercolesAM")
    private String miercolesAM;
    @ColumnInfo(name = "horario_miercolesCM")
    private String miercolesCM;
    @ColumnInfo(name = "horario_miercolesAT")
    private String miercolesAT;
    @ColumnInfo(name = "horario_miercolesCT")
    private String miercolesCT;

    @ColumnInfo(name = "horario_juevesAM")
    private String juevesAM;
    @ColumnInfo(name = "horario_juevesCM")
    private String juevesCM;
    @ColumnInfo(name = "horario_juevesAT")
    private String juevesAT;
    @ColumnInfo(name = "horario_juevesCT")
    private String juevesCT;

    @ColumnInfo(name = "horario_viernesAM")
    private String viernesAM;
    @ColumnInfo(name = "horario_viernesCM")
    private String viernesCM;
    @ColumnInfo(name = "horario_viernesAT")
    private String viernesAT;
    @ColumnInfo(name = "horario_viernesCT")
    private String viernesCT;

    @ColumnInfo(name = "horario_sabadoAM")
    private String sabadoAM;
    @ColumnInfo(name = "horario_sabadoCM")
    private String sabadoCM;
    @ColumnInfo(name = "horario_sabadoAT")
    private String sabadoAT;
    @ColumnInfo(name = "horario_sabadoCT")
    private String sabadoCT;

    @ColumnInfo(name = "horario_domingoAM")
    private String domingoAM;
    @ColumnInfo(name = "horario_domingoCM")
    private String domingoCM;
    @ColumnInfo(name = "horario_domingoAT")
    private String domingoAT;
    @ColumnInfo(name = "horario_domingoCT")
    private String domingoCT;


    //Setters and Getters

    public String getLunesAM() {
        return lunesAM;
    }

    public void setLunesAM(String lunesAM) {
        this.lunesAM = lunesAM;
    }

    public String getLunesCM() {
        return lunesCM;
    }

    public void setLunesCM(String lunesCM) {
        this.lunesCM = lunesCM;
    }

    public String getLunesAT() {
        return lunesAT;
    }

    public void setLunesAT(String lunesAT) {
        this.lunesAT = lunesAT;
    }

    public String getLunesCT() {
        return lunesCT;
    }

    public void setLunesCT(String lunesCT) {
        this.lunesCT = lunesCT;
    }

    public String getMartesAM() {
        return martesAM;
    }

    public void setMartesAM(String martesAM) {
        this.martesAM = martesAM;
    }

    public String getMartesCM() {
        return martesCM;
    }

    public void setMartesCM(String martesCM) {
        this.martesCM = martesCM;
    }

    public String getMartesAT() {
        return martesAT;
    }

    public void setMartesAT(String martesAT) {
        this.martesAT = martesAT;
    }

    public String getMartesCT() {
        return martesCT;
    }

    public void setMartesCT(String martesCT) {
        this.martesCT = martesCT;
    }

    public String getMiercolesAM() {
        return miercolesAM;
    }

    public void setMiercolesAM(String miercolesAM) {
        this.miercolesAM = miercolesAM;
    }

    public String getMiercolesCM() {
        return miercolesCM;
    }

    public void setMiercolesCM(String miercolesCM) {
        this.miercolesCM = miercolesCM;
    }

    public String getMiercolesAT() {
        return miercolesAT;
    }

    public void setMiercolesAT(String miercolesAT) {
        this.miercolesAT = miercolesAT;
    }

    public String getMiercolesCT() {
        return miercolesCT;
    }

    public void setMiercolesCT(String miercolesCT) {
        this.miercolesCT = miercolesCT;
    }

    public String getJuevesAM() {
        return juevesAM;
    }

    public void setJuevesAM(String juevesAM) {
        this.juevesAM = juevesAM;
    }

    public String getJuevesCM() {
        return juevesCM;
    }

    public void setJuevesCM(String juevesCM) {
        this.juevesCM = juevesCM;
    }

    public String getJuevesAT() {
        return juevesAT;
    }

    public void setJuevesAT(String juevesAT) {
        this.juevesAT = juevesAT;
    }

    public String getJuevesCT() {
        return juevesCT;
    }

    public void setJuevesCT(String juevesCT) {
        this.juevesCT = juevesCT;
    }

    public String getViernesAM() {
        return viernesAM;
    }

    public void setViernesAM(String viernesAM) {
        this.viernesAM = viernesAM;
    }

    public String getViernesCM() {
        return viernesCM;
    }

    public void setViernesCM(String viernesCM) {
        this.viernesCM = viernesCM;
    }

    public String getViernesAT() {
        return viernesAT;
    }

    public void setViernesAT(String viernesAT) {
        this.viernesAT = viernesAT;
    }

    public String getViernesCT() {
        return viernesCT;
    }

    public void setViernesCT(String viernesCT) {
        this.viernesCT = viernesCT;
    }

    public String getSabadoAM() {
        return sabadoAM;
    }

    public void setSabadoAM(String sabadoAM) {
        this.sabadoAM = sabadoAM;
    }

    public String getSabadoCM() {
        return sabadoCM;
    }

    public void setSabadoCM(String sabadoCM) {
        this.sabadoCM = sabadoCM;
    }

    public String getSabadoAT() {
        return sabadoAT;
    }

    public void setSabadoAT(String sabadoAT) {
        this.sabadoAT = sabadoAT;
    }

    public String getSabadoCT() {
        return sabadoCT;
    }

    public void setSabadoCT(String sabadoCT) {
        this.sabadoCT = sabadoCT;
    }

    public String getDomingoAM() {
        return domingoAM;
    }

    public void setDomingoAM(String domingoAM) {
        this.domingoAM = domingoAM;
    }

    public String getDomingoCM() {
        return domingoCM;
    }

    public void setDomingoCM(String domingoCM) {
        this.domingoCM = domingoCM;
    }

    public String getDomingoAT() {
        return domingoAT;
    }

    public void setDomingoAT(String domingoAT) {
        this.domingoAT = domingoAT;
    }

    public String getDomingoCT() {
        return domingoCT;
    }

    public void setDomingoCT(String domingoCT) {
        this.domingoCT = domingoCT;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdLocal() {
        return idLocal;
    }

    public void setIdLocal(long idLocal) {
        this.idLocal = idLocal;
    }


}
