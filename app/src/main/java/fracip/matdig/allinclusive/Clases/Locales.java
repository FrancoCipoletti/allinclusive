package fracip.matdig.allinclusive.Clases;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Locales {

    //Atributos
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "locales_id")
    private long id;
    @ColumnInfo(name = "Locales_Nombre")
    private String nombre;
    @ColumnInfo(name = "Locales_FechaCreacion")
    private String fechaCreacion;
    @ColumnInfo(name = "Locales_Prioridad")
    private long prioridad;
    @ColumnInfo(name = "Locales_Miembro")
    private boolean miembro;
    //Poner atributo imagen
    @ColumnInfo(name = "Locales_Descripcion")
    private String descripcion;
    @ColumnInfo(name = "Locales_TelefonoFijo")
    private String telefonoFijo;
    @ColumnInfo(name = "Locales_TelefonoMovil")
    private String telefonoMovil;
    @ColumnInfo(name = "Locales_Rubro")
    private String rubro;
    @ColumnInfo(name = "Locales_NivelMiembro")
    private int nivelMiembro;
    @ColumnInfo(name = "Locales_Latitud")
    private String latitud;
    @ColumnInfo(name = "Locales_Longitud")
    private String longitud;

    @ColumnInfo(name = "locales_imgPerfil")
    private String imgPerfil;

    //Constructor
    public Locales() {
    }

    //Setters y Getters
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isMiembro() {
        return miembro;
    }

    public void setMiembro(boolean miembro) {
        this.miembro = miembro;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTelefonoFijo() {
        return telefonoFijo;
    }

    public void setTelefonoFijo(String telefonoFijo) {
        this.telefonoFijo = telefonoFijo;
    }

    public String getTelefonoMovil() {
        return telefonoMovil;
    }

    public void setTelefonoMovil(String telefonoMovil) {
        this.telefonoMovil = telefonoMovil;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRubro() {
        return rubro;
    }

    public void setRubro(String rubro) {
        this.rubro =rubro;
    }

    public void setNivelMiembro(int nivelMiembro) {
        this.nivelMiembro = nivelMiembro;
    }

    public int getNivelMiembro() {
        return nivelMiembro;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public long getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(long prioridad) {
        this.prioridad = prioridad;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getImgPerfil() {
        return imgPerfil;
    }

    public void setImgPerfil(String imgPerfil) {
        this.imgPerfil = imgPerfil;
    }

}
