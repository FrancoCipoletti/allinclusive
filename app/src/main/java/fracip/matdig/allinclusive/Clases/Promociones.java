package fracip.matdig.allinclusive.Clases;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Promociones {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "promociones_id")
    private int id;
    @ColumnInfo(name = "promociones_fechaCreacion")
    private String fechaCreacion;
    @ColumnInfo(name = "promociones_titulo")
    private String titulo;
    @ColumnInfo(name = "promociones_detalle")
    private String detalle;
    @ColumnInfo(name = "promociones_idLocal")
    private int idLocal;
    @ColumnInfo(name = "promociones_prioridad")
    private long prioridad;
    @ColumnInfo(name = "promociones_imgPromocion")
    private String imgPromocion;

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public long getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(long prioridad) {
        this.prioridad = prioridad;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdLocal() {
        return idLocal;
    }

    public void setIdLocal(int idLocal) {
        this.idLocal = idLocal;
    }
    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getImgPromocion() {
        return imgPromocion;
    }

    public void setImgPromocion(String imgPromocion) {
        this.imgPromocion = imgPromocion;
    }
}
