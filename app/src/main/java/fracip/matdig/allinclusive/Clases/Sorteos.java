package fracip.matdig.allinclusive.Clases;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Sorteos {

    //Atributos
    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "sorteos_idLocal")
    private int idLocal;
    @ColumnInfo(name = "sorteos_detalle")
    private String detalle;
    @ColumnInfo(name = "sorteos_prioridad")
    private long prioridad;
    @ColumnInfo(name= "sorteos_titulo")
    private String titulo;
    @ColumnInfo(name= "sorteos_imgSorteos")
    private String imgSorteo;

    //Setters and Getters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdLocal() {
        return idLocal;
    }

    public void setIdLocal(int idLocal) {
        this.idLocal = idLocal;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public long getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(long prioridad) {
        this.prioridad = prioridad;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getImgSorteo() {
        return imgSorteo;
    }

    public void setImgSorteo(String imgSorteo) {
        this.imgSorteo = imgSorteo;
    }



}
