package fracip.matdig.allinclusive.Servicios;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import fracip.matdig.allinclusive.Clases.Horario;
import fracip.matdig.allinclusive.Clases.Locales;
import fracip.matdig.allinclusive.Clases.Promociones;
import fracip.matdig.allinclusive.Clases.Sorteos;
import fracip.matdig.allinclusive.Dao.HorarioDao;
import fracip.matdig.allinclusive.Dao.LocalesDao;
import fracip.matdig.allinclusive.Dao.PromocionesDao;
import fracip.matdig.allinclusive.Dao.SorteosDao;

@Database(entities = {Locales.class, Promociones.class, Sorteos.class, Horario.class},version = 28 , exportSchema = false)
public abstract class DataBase extends RoomDatabase {
    public abstract LocalesDao localesDao();
    public abstract PromocionesDao promocionesDao();
    public abstract SorteosDao sorteosDao();
    public abstract HorarioDao horarioDao();
}
