package fracip.matdig.allinclusive.Servicios;

import android.util.Log;

import java.util.Calendar;
import java.util.List;
import fracip.matdig.allinclusive.Clases.Horario;
import static java.lang.Integer.valueOf;

public class abiertoCerrado {


    public static boolean estaAbierto(List horarioList) {

        if (!horarioList.isEmpty()) {

            Horario horario = (Horario) horarioList.get(0);
            Calendar calendar = Calendar.getInstance();
            int day = calendar.get(Calendar.DAY_OF_WEEK);
            int hora = calendar.get(Calendar.HOUR);
            int min = calendar.get(Calendar.MINUTE);
            int ampm = calendar.get(Calendar.AM_PM);
            switch (ampm) {
                case Calendar.AM:
                    switch (day) {
                        case Calendar.SUNDAY:
                            String[] horaDomAM = horario.getDomingoAM().split(":");
                            String[] horaDomCM = horario.getDomingoCM().split(":");
                            if (hora >= valueOf(horaDomAM[0]) && hora < valueOf(horaDomCM[0]) && min >= valueOf(horaDomAM[1])) {
                                return true;
                            } else {
                                return false;
                            }

                        case Calendar.MONDAY:
                            String[] horaLunAM = horario.getLunesAM().split(":");
                            String[] horaLunCM = horario.getLunesCM().split(":");
                            if (hora >= valueOf(horaLunAM[0]) && hora < valueOf(horaLunCM[0]) && min >= valueOf(horaLunAM[1])) {
                                return true;
                            } else {
                                return false;
                            }

                        case Calendar.TUESDAY:
                            String[] horaMarAM = horario.getMartesAM().split(":");
                            String[] horaMarCM = horario.getMartesCM().split(":");
                            if (hora >= valueOf(horaMarAM[0]) && hora < valueOf(horaMarCM[0]) ) {
                                //&& min >= valueOf(horaMarAM[1])
                                return true;
                            } else {
                                return false;
                            }

                        case Calendar.WEDNESDAY:
                            String[] horaMieAM = horario.getMiercolesAM().split(":");
                            String[] horaMieCM = horario.getMiercolesCM().split(":");
                            if (hora >= valueOf(horaMieAM[0]) && hora < valueOf(horaMieCM[0]) ) {
                                //&& min >= valueOf(horaMieAM[1])
                                return true;
                            } else {
                                return false;
                            }

                        case Calendar.THURSDAY:
                            String[] horaJueAM = horario.getJuevesAM().split(":");
                            String[] horaJueCM = horario.getJuevesCM().split(":");
                            if (hora >= valueOf(horaJueAM[0]) && hora < valueOf(horaJueCM[0]) && min >= valueOf(horaJueAM[1])) {
                                return true;
                            } else {
                                return false;
                            }

                        case Calendar.FRIDAY:
                            String[] horaVieAM = horario.getViernesAM().split(":");
                            String[] horaVieCM = horario.getViernesCM().split(":");
                            if (hora >= valueOf(horaVieAM[0]) && hora < valueOf(horaVieCM[0]) && min >= valueOf(horaVieAM[1])) {
                                return true;
                            } else {
                                return false;
                            }

                        case Calendar.SATURDAY:
                            String[] horaSabAM = horario.getSabadoAM().split(":");
                            String[] horaSabCM = horario.getSabadoCM().split(":");
                            if (hora >= valueOf(horaSabAM[0]) && hora < valueOf(horaSabCM[0]) && min >= valueOf(horaSabAM[1])) {
                                return true;
                            } else {
                                return false;
                            }

                    }
                case Calendar.PM:
                    switch (day) {
                        case Calendar.SUNDAY:
                            String[] horaDomAT = horario.getDomingoAT().split(":");
                            String[] horaDomCT = horario.getDomingoCT().split(":");
                            if (hora >= valueOf(horaDomAT[0]) && hora < valueOf(horaDomCT[0]) && min >= valueOf(horaDomAT[1])) {
                                return true;
                            } else {
                                return false;
                            }

                        case Calendar.MONDAY:
                            String[] horaLunAT = horario.getLunesAT().split(":");
                            String[] horaLunCT = horario.getLunesCT().split(":");
                            if (hora >= valueOf(horaLunAT[0]) && hora < valueOf(horaLunCT[0]) && min >= valueOf(horaLunAT[1])) {
                                return true;
                            } else {
                                return false;
                            }

                        case Calendar.TUESDAY:
                            String[] horaMarAT = horario.getMartesAT().split(":");
                            String[] horaMarCT = horario.getMartesCT().split(":");
                            if (hora >= valueOf(horaMarAT[0]) && hora < valueOf(horaMarCT[0]) && min >= valueOf(horaMarAT[1])) {
                                return true;
                            } else {
                                return false;
                            }

                        case Calendar.WEDNESDAY:
                            String[] horaMieAT = horario.getMiercolesAT().split(":");
                            String[] horaMieCT = horario.getMiercolesCT().split(":");
                            if (hora >= valueOf(horaMieAT[0]) && hora < valueOf(horaMieCT[0]) && min >= valueOf(horaMieAT[1])) {
                                Log.d("asddsa","Abierto: " + horaMieAT[0] + horaMieAT [1] + horaMieCT[0]);
                                return true;
                            } else {
                                Log.d("asddsa","Cerrado: " + horaMieAT[0] + horaMieAT [1] + horaMieCT[0] + horario.getIdLocal());
                                return false;
                            }

                        case Calendar.THURSDAY:
                            String[] horaJueAT = horario.getJuevesAT().split(":");
                            String[] horaJueCT = horario.getJuevesCT().split(":");
                            if (hora >= valueOf(horaJueAT[0]) && hora < valueOf(horaJueCT[0]) && min >= valueOf(horaJueAT[1])) {
                                return true;
                            } else {
                                return false;
                            }

                        case Calendar.FRIDAY:
                            String[] horaVieAT = horario.getViernesAT().split(":");
                            String[] horaVieCT = horario.getViernesCT().split(":");
                            if (hora >= valueOf(horaVieAT[0]) && hora < valueOf(horaVieCT[0]) && min >= valueOf(horaVieAT[1])) {
                                return true;
                            } else {
                                return false;
                            }

                        case Calendar.SATURDAY:
                            String[] horaSabAT = horario.getSabadoAT().split(":");
                            String[] horaSabCT = horario.getSabadoCT().split(":");
                            if (hora >= valueOf(horaSabAT[0]) && hora < valueOf(horaSabCT[0]) && min >= valueOf(horaSabAT[1])) {
                                return true;
                            } else {
                                return false;
                            }

                    }
            }
        }else {Log.d("asddsa","No hay horario ");
            return false;}
        return false;
    }
}

    
