package fracip.matdig.allinclusive.Servicios;

import android.content.Context;
import android.widget.Toast;
import java.util.List;
import fracip.matdig.allinclusive.Actividades.mainActivity;
import fracip.matdig.allinclusive.Clases.Horario;
import fracip.matdig.allinclusive.Clases.Locales;
import fracip.matdig.allinclusive.Clases.Promociones;
import fracip.matdig.allinclusive.Clases.Sorteos;

public class singletonDataBase {

    public static void singletonDB(Context getApplication) {
        String imgTemp="";
        Locales locales = new Locales();
        Promociones promociones =new Promociones();
        Sorteos sorteos =new Sorteos();
        Horario horario =new Horario();
        if (estaVacia()) {
            // Mostrar notificación
            notificarCreandoDB(getApplication);
            // Creación de locales
            crearLocal(1,"Edgar", "Soy la descripcion", "Carnicería", 1,"-32.413386", "-63.237671",34, locales,"");
            crearLocal(2,"TOP", "Soy la descripcion", "Supermercado", 1,"-32.403386", "-63.247671",33,locales,"");
            crearPromocion(1,1,"Choripan 2x1","Soy el detalle ",imgTemp,66, promociones);
            crearPromocion(2,2,"Zapatos Oferton $600","Soy el detalle",imgTemp,65, promociones);
            crearSorteo(1,1,"Compra por $500","Soy el detalle",imgTemp,36,sorteos);
            crearSorteo(2,2,"Compra por $1000","Soy el detalle ",imgTemp,35,sorteos);
            crearHorario(1,1,"08:00","11:59","04:00","08:00","08:00","11:59","04:00","08:00","08:00","11:59","04:00","08:00","08:00","11:59","04:00","08:00","08:00","11:59","04:00","08:00","08:00","11:59","04:00","08:00","08:00","11:59","04:00","08:00",horario);
            crearHorario(2,2,"00:00","08:00","00:00","04:00","00:00","08:00","08:00","11:59","08:00","11:59","00:00","04:00","08:00","11:59","00:00","04:00","08:00","11:59","04:00","08:00","08:00","11:59","04:00","08:00","08:00","11:59","04:00","08:00",horario);
        }
    }
    private static boolean estaVacia() {
        List<Locales> locales1 = mainActivity.databaseLocal.localesDao().getLocales();
        String info = "";
        for (Locales loc : locales1) {
            double locId = loc.getId();
            info = info + locId;
            break;
        }
        if (info.equals("")) {
            return true;
        } else {
            return false;
        }
    }
    private static void notificarCreandoDB(Context getApplication) {
        Toast.makeText(getApplication, "Creando DB", Toast.LENGTH_SHORT).show();
    }
    private static void crearLocal(int id, String Nombre,String Descripcion, String Rubro, int NivelMiembro, String Latitud, String Longitud, long prioridad, Locales locales,String imgPerfil) {
        locales.setId(id);
        locales.setNombre(Nombre);
        locales.setImgPerfil(imgPerfil);
        locales.setDescripcion(Descripcion);
        locales.setRubro(Rubro);
        locales.setNivelMiembro(NivelMiembro);
        locales.setLatitud(Latitud);
        locales.setLongitud(Longitud);
        locales.setPrioridad(prioridad);
        mainActivity.databaseLocal.localesDao().addLocales(locales);
    }

    private static void crearPromocion (int id, int IdLocal, String titulo,String detalle,String imgPromocion, long prioridad, Promociones promociones) {
        promociones.setId(id);
        promociones.setTitulo(titulo);
        promociones.setPrioridad(prioridad);
        promociones.setIdLocal(IdLocal);
        promociones.setDetalle(detalle);
        promociones.setImgPromocion(imgPromocion);
        mainActivity.databaseLocal.promocionesDao().addPromociones(promociones);
    }

    private static void crearSorteo (int id, int idLocal,String titulo, String detalle,String imgSorteo, long prioridad, Sorteos sorteos) {

        //Se crea un sorteo
        sorteos.setId(id);
        sorteos.setIdLocal(idLocal);
        sorteos.setDetalle(detalle);
        sorteos.setImgSorteo(imgSorteo);
        sorteos.setTitulo(titulo);
        sorteos.setPrioridad(prioridad);

        //Se guarda el sorteo en la DB Local
        mainActivity.databaseLocal.sorteosDao().addSorteos(sorteos);

    }

    private static void crearHorario(long id, long idLocal, String lunesAM, String lunesCM, String lunesAT, String lunesCT, String martesAM, String martesCM, String martesAT, String martesCT, String miercolesAM, String miercolesCM, String miercolesAT, String miercolesCT, String juevesAM, String juevesCM, String juevesAT, String juevesCT, String viernesAM, String viernesCM, String viernesAT, String viernesCT, String sabadoAM, String sabadoCM, String sabadoAT, String sabadoCT, String domingoAM, String domingoCM, String domingoAT, String domingoCT, Horario horario) {
        horario.setId(id);
        horario.setIdLocal(idLocal);

        horario.setLunesAM(lunesAM);
        horario.setLunesCM(lunesCM);
        horario.setLunesAT(lunesAT);
        horario.setLunesCT(lunesCT);

        horario.setMartesAM(martesAM);
        horario.setMartesCM(martesCM);
        horario.setMartesAT(martesAT);
        horario.setMartesCT(martesCT);

        horario.setMiercolesAM(miercolesAM);
        horario.setMiercolesCM(miercolesCM);
        horario.setMiercolesAT(miercolesAT);
        horario.setMiercolesCT(miercolesCT);

        horario.setJuevesAM(juevesAM);
        horario.setJuevesCM(juevesCM);
        horario.setJuevesAT(juevesAT);
        horario.setJuevesCT(juevesCT);

        horario.setViernesAM(viernesAM);
        horario.setViernesCM(viernesCM);
        horario.setViernesAT(viernesAT);
        horario.setViernesCT(viernesCT);

        horario.setSabadoAM(sabadoAM);
        horario.setSabadoCM(sabadoCM);
        horario.setSabadoAT(sabadoAT);
        horario.setSabadoCT(sabadoCT);

        horario.setDomingoAM(domingoAM);
        horario.setDomingoCM(domingoCM);
        horario.setDomingoAT(domingoAT);
        horario.setDomingoCT(domingoCT);

        mainActivity.databaseLocal.horarioDao().addHorario(horario);

    }
}