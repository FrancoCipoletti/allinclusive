package fracip.matdig.allinclusive.Actividades;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import android.arch.persistence.room.Room;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v4.view.MenuItemCompat;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.Toast;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.List;
import java.util.Objects;

import fracip.matdig.allinclusive.Adaptadores.adaptadorLocales;
import fracip.matdig.allinclusive.Adaptadores.adaptadorPromociones;
import fracip.matdig.allinclusive.Adaptadores.adaptadorSorteos;
import fracip.matdig.allinclusive.Clases.Horario;
import fracip.matdig.allinclusive.Servicios.DataBase;
import fracip.matdig.allinclusive.Clases.Locales;
import fracip.matdig.allinclusive.Clases.Promociones;
import fracip.matdig.allinclusive.R;
import fracip.matdig.allinclusive.Servicios.singletonDataBase;
import fracip.matdig.allinclusive.Clases.Sorteos;

public class mainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    //TABS
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    //ENDTABS
    private static List<Locales> allLocales;
    public List<Locales> allLocalesStatic;
    private static List<Promociones> allPromociones;
    public List<Promociones> allPromocionesStatic;
    private static List<Sorteos> allSorteos;
    public List<Sorteos> allSorteosStatic;
    private static List<Horario> allHorarios;
    public List<Horario> allHorariosStatic;
    public static DataBase databaseLocal;
    final FirebaseDatabase databaseFB = FirebaseDatabase.getInstance();
    //Busca en referencia "Locales" en DB Online Firebase
    DatabaseReference refLocales = databaseFB.getReference("locales");
    DatabaseReference refPromociones = databaseFB.getReference("promociones");
    DatabaseReference refSorteos = databaseFB.getReference("sorteos");
    DatabaseReference refHorarios = databaseFB.getReference("horarios");
    public String query = "";
    public static adaptadorLocales adaptTabLocales;
    public static boolean isFrag0Creado;
    public static adaptadorPromociones adaptTabPromociones;
    public static boolean isFrag1Creado;
    public static adaptadorSorteos adaptTabSorteos;
    public static boolean isFrag2Creado;
    public static SupportMapFragment adapttab4;
    public static boolean isFrag3Creado;
    public static GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        iniciarDB();
        crearVistas();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void crearVistas() {

        //Toolbar
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //Tabs
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
        //ActionBar
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //Floating Icon
        FloatingActionButton fab = findViewById(R.id.floatingIcon);
        fab.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                //Mensaje
                Toast.makeText(getApplicationContext(), "Sincronizando", Toast.LENGTH_SHORT).show();

                //Locales
                //DB Online Firebase options
                refLocales.keepSynced(true);
                //Funciones de actualizacion y sincronizacion DB online con local
                refLocales.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for(DataSnapshot dSnapshot : dataSnapshot.getChildren()) {
                            int bandera=0;

                            Locales locales = dSnapshot.getValue(Locales.class);
                            assert locales != null;
                            //Por cada local en AllLocalesStatic
                            for (Locales compLocales : allLocalesStatic){
                                //Si encuentra uno con la misma ID que en DBFirebase
                                if (locales.getId() == compLocales.getId()) {
                                    //Lo borra y luego lo aÃ±ade = update
                                    mainActivity.databaseLocal.localesDao().delete(compLocales);
                                    mainActivity.databaseLocal.localesDao().addLocales(locales);
                                    //Pone bandera en 1 para que no lo vuelva a agregar en el siguiente if al salir con break
                                    bandera=1;
                                    break;
                                }
                            }
                            //Si no encontro ninguno con la misma ID entonces lo agrega a la DB Local.
                            if (bandera==0) {
                                mainActivity.databaseLocal.localesDao().addLocales(locales);
                            }
                        }
                        //Trae todos los locales y actualiza los atributos
                        allLocalesStatic= getAllLocales();
                        allLocales=allLocalesStatic;
                        actualizarDatosDeFragmentos();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        System.out.println("The read failed: " + databaseError.getCode());
                    }
                });

                //Promociones
                refPromociones.keepSynced(true);
                //Funciones de actualizacion y sincronizacion DB online con local
                refPromociones.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for(DataSnapshot dSnapshot : dataSnapshot.getChildren()) {
                            int bandera=0;
                            Promociones locales = dSnapshot.getValue(Promociones.class);
                            assert locales != null;

                            //Por cada local en AllLocalesStatic
                            for (Promociones compLocales : allPromocionesStatic){
                                //Si encuentra uno con la misma ID que en DBFirebase
                                if (locales.getId() == compLocales.getId()) {
                                    //Lo borra y luego lo aÃ±ade = update
                                    mainActivity.databaseLocal.promocionesDao().delete(compLocales);
                                    mainActivity.databaseLocal.promocionesDao().addPromociones(locales);
                                    //Pone bandera en 1 para que no lo vuelva a agregar en el siguiente if al salir con break
                                    bandera=1;
                                    break;
                                }
                            }
                            //Si no encontro ninguno con la misma ID entonces lo agrega a la DB Local.
                            if (bandera==0) {
                                mainActivity.databaseLocal.promocionesDao().addPromociones(locales);
                            }
                        }
                        //Trae todos los locales y actualiza los atributos
                        allPromocionesStatic= getAllPromociones();
                        allPromociones=allPromocionesStatic;
                        actualizarDatosDeFragmentos();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        System.out.println("The read failed: " + databaseError.getCode());
                    }
                });

                //Sorteos
                refSorteos.keepSynced(true);
                //Funciones de actualizacion y sincronizacion DB online con local
                refSorteos.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for(DataSnapshot dSnapshot : dataSnapshot.getChildren()) {
                            int bandera=0;
                            Sorteos locales = dSnapshot.getValue(Sorteos.class);
                            assert locales != null;

                            //Por cada local en AllLocalesStatic
                            for (Sorteos compLocales : allSorteosStatic){
                                //Si encuentra uno con la misma ID que en DBFirebase
                                if (locales.getId() == compLocales.getId()) {
                                    //Lo borra y luego lo aÃ±ade = update
                                    mainActivity.databaseLocal.sorteosDao().delete(compLocales);
                                    mainActivity.databaseLocal.sorteosDao().addSorteos(locales);
                                    //Pone bandera en 1 para que no lo vuelva a agregar en el siguiente if al salir con break
                                    bandera=1;
                                    break;
                                }
                            }
                            //Si no encontro ninguno con la misma ID entonces lo agrega a la DB Local.
                            if (bandera==0) {
                                mainActivity.databaseLocal.sorteosDao().addSorteos(locales);
                            }
                        }
                        //Trae todos los locales y actualiza los atributos
                        allSorteosStatic= getAllSorteos();
                        allSorteos=allSorteosStatic;
                        actualizarDatosDeFragmentos();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        System.out.println("The read failed: " + databaseError.getCode());
                    }
                });

                //Horarios
                refHorarios.keepSynced(true);
                //Funciones de actualizacion y sincronizacion DB online con local
                refHorarios.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for(DataSnapshot dSnapshot : dataSnapshot.getChildren()) {
                            int bandera=0;
                            Horario horario = dSnapshot.getValue(Horario.class);
                            assert horario != null;
                            //Por cada local en AllLocalesStatic
                            for (Horario compHorario : allHorariosStatic){
                                //Si encuentra uno con la misma ID que en DBFirebase
                                if (horario.getId() == compHorario.getId()) {
                                    //Lo borra y luego lo aÃ±ade = update
                                    mainActivity.databaseLocal.horarioDao().delete(compHorario);
                                    mainActivity.databaseLocal.horarioDao().addHorario(horario);
                                    //Pone bandera en 1 para que no lo vuelva a agregar en el siguiente if al salir con break
                                    bandera=1;
                                    break;
                                }
                            }
                            //Si no encontro ninguno con la misma ID entonces lo agrega a la DB Local.
                            if (bandera==0) {
                                mainActivity.databaseLocal.horarioDao().addHorario(horario);
                            }
                        }
                        //Trae todos los locales y actualiza los atributos
                        allHorariosStatic=getAllHorarios();
                        allHorarios=allHorariosStatic;
                        allLocalesStatic= getAllLocales();
                        allLocales=allLocalesStatic;
                        actualizarDatosDeFragmentos();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        System.out.println("The read failed: " + databaseError.getCode());
                    }
                });

            }
        });
    }

    private void iniciarDB() {
        //DB Local
        databaseLocal = Room.databaseBuilder(getApplicationContext(), DataBase.class,"db")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build();
        singletonDataBase.singletonDB(getApplicationContext());
        //Inicia variables con objetos en DB local
        allLocales = getAllLocales();
        allLocalesStatic = allLocales;
        allPromociones = getAllPromociones();
        allPromocionesStatic = allPromociones;
        allSorteos = getAllSorteos();
        allSorteosStatic = allSorteos;
        allHorarios = getAllHorarios();
        allHorariosStatic = allHorarios;

        int count = 0;
        for (Horario item : getAllHorarios()) {
            Log.d("asddsa","Horario.IdLocal: " + item.getIdLocal());

            count = count + 1 ;
        }
        Log.d("asddsa","cnatidad hroarios: " + count);
        count=0;
        for (Locales item : getAllLocales()) {
            Log.d("asddsa","Nombre.Local: " + item.getNombre());
            count = count + 1 ;
        }
        Log.d("asddsa","cnatidad locales: " + count);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    Menu menu1 = null;
    MenuItem search1 = null;
    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        final MenuItem searchItem = menu.findItem(R.id.search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setQueryHint("");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                setQuer(s);
                if (!getQuer().equals("")) {
                    searchItem.collapseActionView();
                }
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s){
                setQuer(s);
                if (getQuer().equals(""))
                {
                    allLocales = allLocalesStatic;
                    allPromociones = allPromocionesStatic;
                    actualizarDatosDeFragmentos();
                    Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.app_name);
                }
                if (!getQuer().equals("")) {
                    filtrar();
                }
                return false;
            }

        });

        return true;

    }

    private void setItemsVisibility(Menu menu, MenuItem exception, boolean visible) {
        for (int i=0; i<menu.size(); ++i) {
            MenuItem item = menu.getItem(i);
            if (item != exception) item.setVisible(visible);
        }
    }

    private void filtrar() {
        allLocales = mainActivity.databaseLocal.localesDao().getLocalesPorNombre("%"+getQuer()+"%");
        allPromociones = mainActivity.databaseLocal.promocionesDao().getPromocionesPorNombreYRubro("%"+getQuer()+"%");
        actualizarDatosDeFragmentos();
    }

    private void actualizarDatosDeFragmentos() {
        if (isFrag0Creado()){
            adaptTabLocales.setData(allLocales);
            adaptTabLocales.notifyDataSetChanged();
        }
        if (isFrag1Creado()){
            adaptTabPromociones.setData(allPromociones);
            adaptTabPromociones.notifyDataSetChanged();
        }
        if (isFrag2Creado()) {
            adaptTabSorteos.setData(allSorteos);
            adaptTabSorteos.notifyDataSetChanged();
        }
        if (isFrag3Creado()) {
            mMap.clear();
            generarMarcadores(getApplicationContext());
        }

    }

    private static void generarMarcadores(Context context) {
        for (Locales compLocales : allLocales) {
            if (compLocales.getLongitud()!=null) {
                double latitud = Double.parseDouble(compLocales.getLatitud());
                double longitud = Double.parseDouble(compLocales.getLongitud());
                LatLng LatLng = new LatLng(latitud, longitud);
                mMap.addMarker((new MarkerOptions()
                        .position(LatLng)
                        .title(compLocales.getRubro()+" "+compLocales.getNombre())
                        .icon(bitmapDescriptorFromVector(context, R.drawable.ic_pin))));
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
            if (id == R.id.nav_compartir) {
            } else if (id == R.id.nav_send) {
            } else if (id == R.id.nav_almacenes) {
                setQuer("Almacén");
                Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.nav_almacenes);
                filtrar();
                 
            } else if (id == R.id.nav_automotores) {
                setQuer("Automotor");
                Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.nav_automotores);
                filtrar();
                 
            } else if (id == R.id.nav_cafeterias) {
                setQuer("Cafetería");
                Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.nav_cafeterias);
                filtrar();
                 
            } else if (id == R.id.nav_carnicerias) {
                setQuer("Carnicería");
                Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.nav_carnicerias);
                filtrar();
                 
            } else if (id == R.id.nav_cerrajerias) {
                setQuer("Cerrajería");
                Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.nav_cerrajerias);
                filtrar();
                 
            } else if (id == R.id.nav_clubes) {
                setQuer("Club");
                Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.nav_clubes);
                filtrar();
                 
            } else if (id == R.id.nav_estacionesdeservicio) {
                setQuer("Estación de Servicio");
                Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.nav_estacionesdeservicio);
                filtrar();
                 
            } else if (id == R.id.nav_farmacias) {
                setQuer("Farmacia");
                Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.nav_farmacias);
                filtrar();
                 
            } else if (id == R.id.nav_ferreterias) {
                setQuer("Ferretería");
                Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.nav_ferreterias);
                filtrar();
                 
            } else if (id == R.id.nav_florerias) {
                setQuer("Florería");
                Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.nav_florerias);
                filtrar();
                 
            } else if (id == R.id.nav_heladerias) {
                setQuer("Heladería");
                Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.nav_heladerias);
                filtrar();
                 
            } else if (id == R.id.nav_hoteles) {
                setQuer("Hotel");
                Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.nav_hoteles);
                filtrar();
                 
            } else if (id == R.id.nav_inmobiliarias) {
                setQuer("Inmobiliria");
                Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.nav_inmobiliarias);
                filtrar();
                 
            } else if (id == R.id.nav_instituciones) {
                setQuer("Institución");
                Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.nav_instituciones);
                filtrar();
                 
            } else if (id == R.id.nav_kioscos) {
                setQuer("Kiosco");
                Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.nav_kioscos);
                filtrar();
                 
            } else if (id == R.id.nav_lavanderias) {
                setQuer("Lavandería");
                Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.nav_lavanderias);
                filtrar();
                 
            } else if (id == R.id.nav_limpieza) {
                setQuer("Limpieza");
                Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.nav_limpieza);
                filtrar();
                 
            } else if (id == R.id.nav_opticas) {
                setQuer("Óptica");
                Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.nav_opticas);
                filtrar();
                 
            } else if (id == R.id.nav_panaderias) {
                setQuer("Panadería");
                Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.nav_panaderias);
                filtrar();
                 
            } else if (id == R.id.nav_peluquerias) {
                setQuer("Peluquería");
                Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.nav_peluquerias);
                filtrar();
                 
            } else if (id == R.id.nav_pinturerias) {
                setQuer("Pinturería");
                Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.nav_pinturerias);
                filtrar();
                 
            } else if (id == R.id.nav_pizzerias) {
                setQuer("Pizzería");
                Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.nav_pizzerias);
                filtrar();
                 
            } else if (id == R.id.nav_relojerias) {
                setQuer("Relojería");
                Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.nav_relojerias);
                filtrar();
                 
            } else if (id == R.id.nav_remisses) {
                setQuer("Remisería");
                Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.nav_remisses);
                filtrar();
                 
            } else if (id == R.id.nav_restaurantes) {
                setQuer("Restaurant");
                Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.nav_restaurantes);
                filtrar();
                 
            } else if (id == R.id.nav_rotiserias) {
                setQuer("Rotisería");
                Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.nav_rotiserias);
                filtrar();
                 
            } else if (id == R.id.nav_supermercados) {
                setQuer("Supermercado");
                Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.nav_supermercados);
                filtrar();
                 
            } else if (id == R.id.nav_terminal) {
                setQuer("Terminal");
                Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.nav_terminal);
                filtrar();
                 
            } else if (id == R.id.nav_transportemercaderia) {
                setQuer("Transporte");
                Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.nav_transportemercaderia);
                filtrar();
                 
            } else if (id == R.id.nav_verdulerias) {
                setQuer("Verdulería");
                Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.nav_verdulerias);
                filtrar();
                 
            } else if (id == R.id.nav_veterinarias) {
                setQuer("Veterinaria");
                Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.nav_verdulerias);
                filtrar();
            } else if (id == R.id.nav_zapaterias) {
                setQuer("Zapatería");
                Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.nav_zapaterias);
                filtrar();
            }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
        public String getQuer() {
            return query;
        }
        public void setQuer(String query) {
            this.query = query;
        }

        @Override
        protected void onStart() {
            super.onStart();
        }


    //TABS



    public static class MapFragment extends Fragment implements OnMapReadyCallback{
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        public List<Locales> data;

        public MapFragment() {
        }

        public void setData(List<Locales> data) {
            this.data = data;
        }
        public static MapFragment newInstance(int sectionNumber) {
            MapFragment fragment = new MapFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.activity_maps, null, false);
            SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
            mainActivity.setIsFrag3Creado(true);
            return view;
        }


        @Override
        public void onMapReady(GoogleMap googleMap) {
            mMap = googleMap;
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            mMap.setPadding(200,0,200,0);
            LatLng VillaMaria = new LatLng(-32.413376, -63.237671);

            generarMarcadores(getContext());

            float zoom = 14;
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(VillaMaria, zoom));
            mMap.getUiSettings().setZoomControlsEnabled(false);
            mMap.getUiSettings().setMapToolbarEnabled(true);
            mMap.getUiSettings().setCompassEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);


        }
    }
    private static BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        assert vectorDrawable != null;
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    public static class LocalesFragment extends Fragment {

        private static final String ARG_SECTION_NUMBER = "section_number";

        public LocalesFragment() {
        }

        public static LocalesFragment newInstance(int sectionNumber) {
            LocalesFragment fragment = new LocalesFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.tab1, container, false);
            RecyclerView recyclerView = rootView.findViewById(R.id.rv_recycler_view);
            recyclerView.setHasFixedSize(true);
            adaptadorLocales adapter = new adaptadorLocales(allLocales);
            mainActivity.adaptTabLocales = adapter;
            mainActivity.setFrag0Creado(true);
            recyclerView.setAdapter(adaptTabLocales);
            LinearLayoutManager llm = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(llm);
            return rootView;
        }

    }
    public static class PromocionesFragment extends Fragment {

        private static final String ARG_SECTION_NUMBER = "section_number";


        public PromocionesFragment() {
        }

        public static PromocionesFragment newInstance(int sectionNumber) {
            PromocionesFragment fragment = new PromocionesFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.tab1, container, false);
            RecyclerView recyclerView = rootView.findViewById(R.id.rv_recycler_view);
            recyclerView.setHasFixedSize(true);
            adaptadorPromociones adapter = new adaptadorPromociones(allPromociones);
            mainActivity.adaptTabPromociones = adapter;
            mainActivity.setFrag1Creado(true);
            recyclerView.setAdapter(adaptTabPromociones);
            LinearLayoutManager llm = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(llm);
            return rootView;
        }

    }
    public static class SorteosFragment extends Fragment {

        private static final String ARG_SECTION_NUMBER = "section_number";


        public SorteosFragment() {
        }

        public static SorteosFragment newInstance(int sectionNumber) {
            SorteosFragment fragment = new SorteosFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.tab1, container, false);
            RecyclerView recyclerView = rootView.findViewById(R.id.rv_recycler_view);
            recyclerView.setHasFixedSize(true);
            adaptadorSorteos adapter = new adaptadorSorteos(allSorteos);
            mainActivity.setFrag2Creado(true);
            mainActivity.adaptTabSorteos = adapter;
            recyclerView.setAdapter(adaptTabSorteos);
            LinearLayoutManager llm = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(llm);
            return rootView;
        }

    }
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position) {
                case 0:
                    return LocalesFragment.newInstance(position + 1);
                case 1:
                    return PromocionesFragment.newInstance(position + 1);
                case 2:
                    return SorteosFragment.newInstance(position + 1);
                case 3:
                    return MapFragment.newInstance(position + 1);
            }
            return null;
        }
        @Override
        public int getCount() {
            // Show 3 total pages.
            return 4;
        }
    }

    // Setters and Getters
    public static boolean isFrag3Creado() {
        return isFrag3Creado;
    }

    public static void setIsFrag3Creado(boolean Frag3Creado) {
        isFrag3Creado = Frag3Creado;
    }

    public boolean isFrag0Creado() {
        return isFrag0Creado;
    }

    public static void setFrag0Creado(boolean frag0Creado) {
        isFrag0Creado = frag0Creado;
    }

    public boolean isFrag1Creado() {
        return isFrag1Creado;
    }

    public static void setFrag1Creado(boolean frag1Creado) {
        isFrag1Creado = frag1Creado;
    }

    public boolean isFrag2Creado() {
        return isFrag2Creado;
    }

    public static void setFrag2Creado(boolean frag2Creado) {
        isFrag2Creado = frag2Creado;
    }


    private List<Sorteos> getAllSorteos() {
        List<Sorteos> data = mainActivity.databaseLocal.sorteosDao().getSorteos();
        return data;
    }
    private List<Promociones> getAllPromociones() {
        List<Promociones> data = mainActivity.databaseLocal.promocionesDao().getPromociones();
        return data;
    }
    private List<Locales> getAllLocales() {
        List<Locales> data = mainActivity.databaseLocal.localesDao().getLocales();
        return data;
    }
    private List<Horario> getAllHorarios() {
        List<Horario> data = mainActivity.databaseLocal.horarioDao().getHorario();
        return data;
    }
}