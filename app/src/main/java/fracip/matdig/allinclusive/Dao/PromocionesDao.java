package fracip.matdig.allinclusive.Dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import fracip.matdig.allinclusive.Clases.Promociones;

@Dao
public interface PromocionesDao {
    @Insert
    void addPromociones(Promociones promociones);
    @Query("select * from Promociones order by promociones_prioridad DESC")
    List<Promociones> getPromociones();
    @Delete
    void delete(Promociones promociones);
    @Query("select promociones_id ,promociones_titulo, promociones_prioridad,promociones_idLocal,promociones_detalle,promociones_fechaCreacion from promociones inner join Locales on promociones_idLocal = locales_id where Locales_Nombre like :nombre or Locales_Rubro like :nombre order by promociones_prioridad DESC")
    List<Promociones> getPromocionesPorNombreYRubro(String nombre);
}
