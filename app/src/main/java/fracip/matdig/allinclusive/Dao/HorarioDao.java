package fracip.matdig.allinclusive.Dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import java.util.List;
import fracip.matdig.allinclusive.Clases.Horario;

@Dao
public interface HorarioDao {
    @Insert
    void addHorario(Horario horario);
    @Query("select * from Horario")
    List<Horario> getHorario();
    @Delete
    void delete(Horario horario);
    @Query("SELECT * FROM Horario WHERE horario_idLocal = :idLocal limit 1")
    List<Horario> getHorarioPorIdLocal(long idLocal);
}
