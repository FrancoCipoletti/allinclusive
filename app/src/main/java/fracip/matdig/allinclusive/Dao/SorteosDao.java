package fracip.matdig.allinclusive.Dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import java.util.List;

import fracip.matdig.allinclusive.Clases.Sorteos;

@Dao
public interface SorteosDao {

    @Insert
    void addSorteos(Sorteos sorteos);
    @Query("select * from Sorteos order by sorteos_prioridad DESC")
    List<Sorteos> getSorteos();
    @Delete
    void delete(Sorteos sorteos);
}
