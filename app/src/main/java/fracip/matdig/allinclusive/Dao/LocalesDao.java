package fracip.matdig.allinclusive.Dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import java.util.List;

import fracip.matdig.allinclusive.Clases.Locales;

@Dao
public interface LocalesDao {
    @Insert
    void addLocales(Locales locales);
    @Query("select * from Locales order by Locales_Prioridad DESC")
    List<Locales> getLocales();
    @Delete
    void delete(Locales locales);
    @Query("SELECT * FROM Locales WHERE Locales_Nombre like :filtro or Locales_Rubro like :filtro order by Locales_Prioridad DESC")
    List<Locales> getLocalesPorNombre(String filtro);
    @Query("SELECT * FROM Locales WHERE locales_id = :id")
    List<Locales> getLocalesPorId(int id);
}
