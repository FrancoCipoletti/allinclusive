package fracip.matdig.allinclusive.Adaptadores;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;

import fracip.matdig.allinclusive.Clases.Locales;
import fracip.matdig.allinclusive.Actividades.mainActivity;
import fracip.matdig.allinclusive.Clases.Promociones;
import fracip.matdig.allinclusive.R;
import fracip.matdig.allinclusive.Servicios.base64Decoder;

import static java.lang.Integer.parseInt;

public class adaptadorPromociones extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    public List<Promociones> data;
    public List<Locales> dataLocales;
    static int total_types;

    class ViewHolder0 extends RecyclerView.ViewHolder {
        //ImageView img;
        TextView nombreLocal, rubroLocal, tituloPromocion, detallePromocion;
        ImageView imgAvatar,imgPromocion;

        public ViewHolder0(View itemView) {
            super(itemView);
            nombreLocal = itemView.findViewById(R.id.primary_text);
            rubroLocal = itemView.findViewById(R.id.sub_text);
            tituloPromocion = itemView.findViewById(R.id.title_text);
            detallePromocion = itemView.findViewById(R.id.detail_text);
            imgAvatar = itemView.findViewById(R.id.avatar_image);
            imgPromocion = itemView.findViewById(R.id.media_image);
        }
    }
    @Override
    public int getItemViewType(int position) {
                return 0;
        }

    public adaptadorPromociones(List<Promociones> data) {
        this.data = data;
        total_types = data.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

                return new ViewHolder0(LayoutInflater.from(parent.getContext()).inflate(R.layout.promocion_sorteo, parent, false));
    }
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder,final int position) {
        Promociones promocion = data.get(position);
        if (promocion != null) {
            int idLocal = promocion.getIdLocal();
            setDataLocales(mainActivity.databaseLocal.localesDao().getLocalesPorId(idLocal));
            if (!getDataLocales().isEmpty()){
            Locales local = getDataLocales().get(0);
                switch (holder.getItemViewType()) {
                    case 0:
                        ViewHolder0 viewHolder0 = (ViewHolder0) holder;
                        ((ViewHolder0) holder).imgAvatar.setImageBitmap(base64Decoder.getImageFromBase64(local.getImgPerfil()));
                        ((ViewHolder0) holder).imgPromocion.setImageBitmap(base64Decoder.getImageFromBase64(promocion.getImgPromocion()));
                        ((ViewHolder0) holder).tituloPromocion.setText(promocion.getTitulo());
                        ((ViewHolder0) holder).detallePromocion.setText(promocion.getDetalle());
                        ((ViewHolder0) holder).nombreLocal.setText(local.getNombre());
                        ((ViewHolder0) holder).rubroLocal.setText(local.getRubro());
                        break;

                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<Promociones> data) {
        this.data = data;
    }

    public List<Locales> getDataLocales() {
        return dataLocales;
    }

    public void setDataLocales(List<Locales> dataLocales) {
        this.dataLocales = dataLocales;
    }

}