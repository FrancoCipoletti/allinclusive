package fracip.matdig.allinclusive.Adaptadores;


import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import fracip.matdig.allinclusive.Actividades.mainActivity;
import fracip.matdig.allinclusive.Clases.Horario;
import fracip.matdig.allinclusive.Clases.Locales;
import fracip.matdig.allinclusive.R;
import fracip.matdig.allinclusive.Servicios.base64Decoder;
import fracip.matdig.allinclusive.Servicios.abiertoCerrado;

import static java.lang.Integer.valueOf;

public class adaptadorLocales extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    public List<Locales> data;
    public List<Horario> dataHorario;
    static long total_types;

    class ViewHolder0 extends RecyclerView.ViewHolder {
        //ImageView img;
        TextView Nombre;
        TextView Rubro;
        ImageView imgAvatar;
        public ViewHolder0(View itemView){
            super(itemView);
            imgAvatar = itemView.findViewById(R.id.avatar_image);
            Nombre = itemView.findViewById(R.id.primary_text);
            Rubro = itemView.findViewById(R.id.sub_text);
        }
    }

    class ViewHolder1 extends RecyclerView.ViewHolder {
        //ImageView img;
        TextView Nombre;
        TextView Descripcion;
        TextView Rubro;
        ImageView imgAvatar;
        public ViewHolder1(View itemView){
            super(itemView);
            Nombre = itemView.findViewById(R.id.primary_text);
            Descripcion = itemView.findViewById(R.id.sub_text);
            Rubro = itemView.findViewById(R.id.sub_text2);
            imgAvatar = itemView.findViewById(R.id.media_image);
        }
    }

    class ViewHolder2 extends RecyclerView.ViewHolder {
        TextView Nombre;
        TextView Rubro;
        ImageView imgAvatar;

        public ViewHolder2(View itemView) {
            super(itemView);
            Nombre = itemView.findViewById(R.id.primary_text);
            Rubro = itemView.findViewById(R.id.sub_text);
            imgAvatar = itemView.findViewById(R.id.media_image);
        }
    }

    @Override
    public int getItemViewType(int position) {
        switch (data.get(position).getNivelMiembro()) {
            case 0:
                return data.get(position).getNivelMiembro();
            case 1:
                return data.get(position).getNivelMiembro();
            case 2:
                return data.get(position).getNivelMiembro();
            default:
                return 0;
        }
    }
    public adaptadorLocales(List<Locales> data) {
        this.data = data;
        total_types = data.size();
    }


    public void setData(List<Locales> data) {
        this.data = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 0: return new ViewHolder0(LayoutInflater.from(parent.getContext()).inflate(R.layout.miembro_0, parent, false));
            case 1: return new ViewHolder1(LayoutInflater.from(parent.getContext()).inflate(R.layout.miembro_1, parent, false));
            case 2: return new ViewHolder2(LayoutInflater.from(parent.getContext()).inflate(R.layout.miembro_2, parent, false));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder,final int position) {

        Locales locales = data.get(position);
        boolean estaAbierto = false;

        if (locales != null) {
            long idLocal = locales.getId();
            setDataHorario(mainActivity.databaseLocal.horarioDao().getHorarioPorIdLocal(idLocal));
            Log.d("asddsa", "idlocal: " + idLocal);
            if (!getDataHorario().isEmpty()) {
                Log.d("asddsa", "dataHorario is not empty");
                estaAbierto = abiertoCerrado.estaAbierto(mainActivity.databaseLocal.horarioDao().getHorarioPorIdLocal(idLocal));
            }


            switch (holder.getItemViewType()) {
                case 0:
                    ViewHolder0 viewHolder0 = (ViewHolder0) holder;
                    ((ViewHolder0) holder).imgAvatar.setImageBitmap(base64Decoder.getImageFromBase64(locales.getImgPerfil()));
                    ((ViewHolder0) holder).Nombre.setText(locales.getNombre());
                    ((ViewHolder0) holder).Rubro.setText(locales.getRubro());
                    break;
                case 1:
                    ViewHolder1 viewHolder1 = (ViewHolder1) holder;
                    ((ViewHolder1) holder).imgAvatar.setImageBitmap(base64Decoder.getImageFromBase64(locales.getImgPerfil()));
                    ((ViewHolder1) holder).Nombre.setText(locales.getNombre());
                    if (estaAbierto) {
                        ((ViewHolder1) holder).Descripcion.setText(R.string.abierto);
                    } else {
                        ((ViewHolder1) holder).Descripcion.setText(R.string.cerrado);
                    }
                    ((ViewHolder1) holder).Rubro.setText(locales.getRubro());
                    break;
                case 2:
                    ViewHolder2 viewHolder2 = (ViewHolder2) holder;
                    ((ViewHolder2) holder).imgAvatar.setImageBitmap(base64Decoder.getImageFromBase64(locales.getImgPerfil()));
                    ((ViewHolder2) holder).Nombre.setText(locales.getNombre());
                    ((ViewHolder2) holder).Rubro.setText(locales.getRubro());
                    break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public List<Horario> getDataHorario() {
        return dataHorario;
    }

    public void setDataHorario(List<Horario> dataHorario) {
        this.dataHorario = dataHorario;
    }

}